﻿using System;
using System.ServiceProcess;

namespace TestApplicationWinService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using (var service = new TestSevice())
            {
                ServiceBase.Run(service);
            }
        }
    }
}
